#include <stdbool.h>
#include <string.h>
#include "conversions.h"

void util_asctohex(char a, char *s)
{
    char c;
    c = (a >> 4) & 0x0f;
    if (c <= 9)
        c += '0';
    else
        c += 'a' - 10;
    *s++ = c;
    c = a & 0x0f;
    if (c <= 9)
        c += '0';
    else
        c += 'a' - 10;
    *s++ = c;
    *s = 0;
}

uint8_t util_asc2i(char sign)
{
    uint8_t Byte = 0xff;

    if ((sign >= '0') && (sign <= '9'))
        Byte = sign - '0';
    else
    {
        if ((sign >= 'a') && (sign <= 'f'))
            Byte = sign - ('a' - 10);
        else
        {
            if ((Byte >= 'A') && (Byte <= 'F'))
                Byte = sign - ('A' - 10);
        }
    }
    return Byte;
}

/**
 * itoa() implementation as we don't have a normal std library
 */
char* util_itoa(int value, char* result, int base)
{
    // check that the base is valid
    if (base < 2 || base > 36)
    {
        *result = '\0';
        return result;
    }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do
    {
        tmp_value = value;
        value /= base;
        *ptr++ =
                "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35
                        + (tmp_value - value * base)];
    } while (value);

    // Apply negative sign
    if (tmp_value < 0)
        *ptr++ = '-';
    *ptr-- = '\0';
    while (ptr1 < ptr)
    {
        tmp_char = *ptr;
        *ptr-- = *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}

int util_atoi(const char *str)
{
    char * lptr = (char*) str;
    int result = 0;
    int neg = 1;
    while (1)
    {
        if ((*lptr >= '0') && (*lptr <= '9'))
        {
            result *= 10;
            result += *lptr - '0';
            lptr++;
        }
        else if (*lptr == '-')
        {
            neg *= -1;
            lptr++;
        }
        else
        {
            break;
        }
    }
    return result * neg;
}

int util_atox(const char *str)
{
    char * lptr = (char*) str;
    int result = 0;
    int neg = 1;
    while (1)
    {
        if ((*lptr >= '0') && (*lptr <= '9'))
        {
            result *= 16;
            result += *lptr - '0';
            lptr++;
        }
        else if ((*lptr >= 'a') && (*lptr <= 'f'))
        {
            result *= 16;
            result += *lptr - 'a' + 10;
            lptr++;
        }
        else if ((*lptr >= 'A') && (*lptr <= 'F'))
        {
            result *= 16;
            result += *lptr - 'a' + 10;
            lptr++;
        }
        else if (*lptr == '-')
        {
            neg *= -1;
            lptr++;
        }
        else
        {
            break;
        }
    }
    return result * neg;
}

void util_shift_buf_left(char* buf, size_t size)
{
    if (size <= 1)
    {
        // nothing to do
        return;
    }
    char first = buf[0];
    memcpy(&buf[0], &buf[1], size - 1);
    buf[size - 1] = first;
}
