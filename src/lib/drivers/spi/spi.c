/**
 *****************************************************************************
 * @title   SPI_Master.c
 * @author  Daniel Schnell <dschnell@posteo.de>
 *******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdbool.h>

#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_dma.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_syscfg.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_spi.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "logger.h"
#include "spi.h"
#include "spi_priv.h"
#include "spi_irq.h"
#include "spi_dma.h"
#include "spi_host_com.h"

/*
 * Defines
 */

/*
 * Structure definitions
 */

/*
 * Globals
 */

//
// Definitions for SPI1 & SPI2
//
static volatile DiagSPI spi1_h =
{
    .SPIx = SPI1, .rx_cnt = 0, .tx_cnt = 0,
    .tx_in_use = false, .rx_in_use = false,
    .rx_pos = 0, .tx_pos = 0, .rx_buf = NULL,
    .GPIOx = GPIOB,
    .GPIO_Pins = GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5,
    .GPIOx_nss = GPIOA,
    .NSS = GPIO_Pin_15,
    .is_partial_transfer = false, .is_master = false,
    .transfer_buf_ptr = NULL, .transfer_buf_size = 0,
#ifdef SPI_USES_DMA
    .NVIC_IRQChannel = DMA1_Channel2_3_IRQn, .NVIC_IRQChannelPrio = 1,
    .dma_addr = (uint32_t) &(SPI1 ->DR), .dma_channel_rx = DMA1_Channel2,
    .dma_channel_tx = DMA1_Channel3 ,
#else
    .NVIC_IRQChannel = SPI1_IRQn,
    .NVIC_IRQChannelPrio = 0,
#endif
    };

static volatile DiagSPI spi2_h =
{
    .SPIx = SPI2, .rx_cnt = 0, .tx_cnt = 0,
    .tx_in_use = false, .rx_in_use = false,
    .rx_pos = 0, .tx_pos = 0, .rx_buf = NULL,
    .GPIOx = GPIOB,
    .GPIO_Pins = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15,
    .GPIOx_nss = GPIOB,
    .NSS = GPIO_Pin_12,
    .is_partial_transfer = true, .is_master = true, .transfer_buf_ptr = NULL,
    .transfer_buf_size = 0,
#ifdef SPI_USES_DMA
    .NVIC_IRQChannel = DMA1_Channel4_5_6_7_IRQn, .NVIC_IRQChannelPrio = 2,
    .dma_addr = (uint32_t) (&SPI2 ->DR), .dma_channel_rx = DMA1_Channel4,
    .dma_channel_tx = DMA1_Channel5 ,
#else
    .NVIC_IRQChannel = SPI2_IRQn,
    .NVIC_IRQChannelPrio = 1,
#endif
    };

volatile DiagSPI* spi1 = &spi1_h;
volatile DiagSPI* spi2 = &spi2_h;

/*
 *  Static function definitions
 */

static void spi_init(DiagSPI* spi)
{
    // create binary semaphores for sync ISR->TASK and make immediately empty
    vSemaphoreCreateBinary(spi->rx_sem);
    if (xSemaphoreTake(spi->rx_sem, ( portTickType ) 0) != pdTRUE)
    {
        log_msg("%s: semaphore problem!", __FUNCTION__);
    }
    vSemaphoreCreateBinary(spi->tx_sem);
    if (xSemaphoreTake(spi->tx_sem, ( portTickType ) 0) != pdTRUE)
    {
        log_msg("%s: semaphore (2) problem!", __FUNCTION__);
    }
    spi->lock = xSemaphoreCreateRecursiveMutex();

    // set GPIO pins
    GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef SPI_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure_NSS;
    GPIOSpeed_TypeDef speed = GPIO_Speed_Level_3;

    // Chip select: NSS
    GPIO_InitStructure_NSS.GPIO_Pin = spi->NSS;
    GPIO_InitStructure_NSS.GPIO_OType = GPIO_OType_PP;
    if(spi->is_master)
    {
       GPIO_InitStructure_NSS.GPIO_Mode = GPIO_Mode_OUT;
       GPIO_InitStructure_NSS.GPIO_PuPd = GPIO_PuPd_NOPULL;
    }
    else
    {
        //GPIO_InitStructure_NSS.GPIO_Mode = GPIO_Mode_AF;
        GPIO_InitStructure_NSS.GPIO_Mode = GPIO_Mode_IN;
        GPIO_InitStructure_NSS.GPIO_PuPd = GPIO_PuPd_NOPULL;
    }

    GPIO_InitStructure_NSS.GPIO_Speed = speed;
    GPIO_Init(spi->GPIOx_nss, &GPIO_InitStructure_NSS);

    if (spi->is_master)
    {
        spi_chip_deselect(spi->SPIx);
    }

    // Pins SCK, MISO, MOSI
    GPIO_InitStructure.GPIO_Pin = spi->GPIO_Pins;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_InitStructure.GPIO_Speed = speed;
    GPIO_Init(spi->GPIOx, &GPIO_InitStructure);

    SPI_I2S_DeInit(spi->SPIx);
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;

    SPI_InitStructure.SPI_CRCPolynomial = 0;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;

    if (spi->is_master)
    {
        SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
        SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
        //SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
        SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
        //SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
    }
    else
    {
        SPI_InitStructure.SPI_Mode = SPI_Mode_Slave;
        SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
    }

    SPI_Init(spi->SPIx, &SPI_InitStructure);

    if (false == spi->is_master)
    {
        // Experimental settings
        //SPI_SSOutputCmd(spi->SPIx, DISABLE);
        //SPI_BiDirectionalLineConfig(spi->SPIx, SPI_Direction_Rx);
    }
#ifdef SPI_USES_DMA
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    spi_dma_irq_init(spi);
#else
    spi_irq_init(spi);
#endif

    /* Initialize the FIFO threshold */
    SPI_RxFIFOThresholdConfig(spi->SPIx, SPI_RxFIFOThreshold_QF );

    SPI_Cmd(spi->SPIx, ENABLE);

    if (false == spi->is_master)
    {
        spi_host_com_activate_request_port();
        spi_host_com_set_imx_request_passive();
    }
}

void spi_disable(void* spi)
{

#ifdef SPI_USES_DMA
    spi_dma_tx_enable((DiagSPI*) spi, false);
    spi_dma_rx_enable((DiagSPI*) spi, false);
#endif
    spi_rx_fifo_clear(spi);
    SPI_Cmd(((DiagSPI*) spi)->SPIx, DISABLE);
}

void spi_enable(void* spi)
{
#ifdef SPI_USES_DMA
    spi_dma_tx_enable((DiagSPI*) spi, true);
    spi_dma_rx_enable((DiagSPI*) spi, true);
#endif
    SPI_Cmd(((DiagSPI*) spi)->SPIx, ENABLE);
    spi_rx_fifo_clear(spi);
}

/**
 * Clears SPI RX Fifo
 */
void spi_rx_fifo_clear(DiagSPI* spi)
{
    // empty RxFIFO
    while (SPI_ReceptionFIFOStatus_Empty
            != SPI_GetReceptionFIFOStatus(spi->SPIx))
    {
        SPI_ReceiveData8(spi->SPIx);
    }
}

/**
 * Write to SPI: private main function.
 *
 * If a transfer is ongoing this call spins until the former transfer is finished.
 *
 * @param spi_inst  SPI instance returned from spi_open()
 * @param buf       buffer that should be transfered
 * @param cnt       number of bytes that should be transfered
 *
 */
static ssize_t spi_write_private(void* spi_inst, const char* buf, size_t cnt,
        int ticks)
{
    DiagSPI* spi = (DiagSPI*) spi_inst;
    if ((0 == cnt) || (NULL == spi))
        return 0;

    if (cnt > 65535)
    {
        /* we cannot write more than 64kb at once */
        return -1;
    }

    spi->tx_in_use = true;
    spi->rx_in_use = false;
    spi->tx_buf = (char*) buf;
    spi->rx_cnt = 0;
    spi->tx_cnt = cnt;
    spi->rx_pos = 0;
    spi->tx_pos = 0;

    if (spi->is_master)
    {
        spi_chip_select(spi->SPIx);
    }
    else
    {
        /* wait until a previous ongoing transfer is finished by master */
        while (spi_is_cs_active((void*) spi))
        {
            ;
        }
    }

#ifdef SPI_USES_DMA
    spi_dma_tx_init(spi, buf, cnt);
#else
    spi_tx_isr_enable(spi->SPIx);
#endif

    ssize_t rv;

    if (false == spi->is_master)
    {
        /* set Slave RDY signal */
        spi_host_com_set_imx_request_active();
    }

    /* wait until ISR has finished transfer (synchronous write) */
    if (xSemaphoreTake(spi->tx_sem, (portTickType) ticks) != pdTRUE)
    {
#ifdef SPI_USES_DMA
        spi_dma_tx_enable(spi, false);
#else
        spi_tx_isr_disable(spi->SPIx);
#endif
        rv = -1;
    }
    else
    {
        rv = spi->tx_pos;
    }

    if (false == spi->is_master)
    {
        spi_host_com_set_imx_request_passive();
    }
    else if (false == spi->is_partial_transfer)
    {
        /* a partial transfer holds the chip select line enabled */
        spi_chip_deselect(spi->SPIx);
    }

    spi->tx_cnt = 0;
    spi->tx_in_use = false;
    /* reset transfer buffer: it has to be set every time before receiving */
    spi->transfer_buf_ptr = NULL;
    spi->transfer_buf_size = 0;

    return rv;
}

/**
 * Public function definitions
 */

void spi_chip_select(SPI_TypeDef* SPIx)
{
    if (SPIx == SPI1 )
    {
        GPIO_WriteBit(GPIOA, GPIO_Pin_15, Bit_RESET);
    }
    else if (SPIx == SPI2 )
    {
        GPIO_WriteBit(GPIOB, GPIO_Pin_12, Bit_RESET);
    }
}

void spi_chip_deselect(SPI_TypeDef* SPIx)
{
    if (SPIx == SPI1 )
    {
        GPIO_WriteBit(GPIOA, GPIO_Pin_15, Bit_SET);
    }
    else if (SPIx == SPI2 )
    {
        GPIO_WriteBit(GPIOB, GPIO_Pin_12, Bit_SET);
    }
}

// Initialize SPI1-Interface
void init_spi1()
{
    spi_init(spi1);
}

// Initialize SPI2-Interface
void init_spi2()
{
    spi_init(spi2);
}

/**
 * Open an instance of SPI. The returned handle can be used for functions spi_write() and
 * spi_read() to communicate over SPI.
 *
 * @param   instance        1 for SPI1, 2 for SPI2
 *
 * @return  Pointer to the instance object or NULL if no valid instance number given.
 */
void* spi_open(int instance)
{
    volatile void* rv = NULL;
    switch (instance)
    {
    case SPI_HOST2DIAG_COM_PORT:
        rv = spi1;
        break;
    case SPI_DIAG_MEM_PORT:
        rv = spi2;
        break;
    default:
        break;
    }

    return (void*) rv;
}

/**
 * Write to SPI.
 *
 * If a concurrent transfer to the SPI device is already ongoing, this call
 * spins until the ongoing transfer is finished.
 *
 * @param spi_inst  SPI instance returned from spi_open()
 * @param buf       buffer that should be transfered
 * @param cnt       number of bytes that should be transfered
 * @param ticks     Timeout in system ticks
 *
 * @note    This is a synchronous function, i.e. function waits until command has completed.
 *
 */
ssize_t spi_write(void* spi_inst, const char* buf, size_t cnt, int ticks)
{
    DiagSPI* spi = (DiagSPI*) spi_inst;

    if (xSemaphoreTakeRecursive(spi->lock, ( portTickType ) ticks) != pdTRUE)
    {
        return -1;
    }

    if (false == spi->is_partial_transfer)
    {
        if (spi->is_master)
        {
            spi_chip_deselect(spi->SPIx);
        }
    }
    spi->is_partial_transfer = false;
    ssize_t rv = spi_write_private(spi_inst, buf, cnt, ticks);

    xSemaphoreGiveRecursive(spi->lock);
    return rv;
}

/**
 * Write to SPI && let slave select (NSS line) active.
 *
 * This function is used for sending the first part of a write sequence to a
 * SPI device.
 * After this call the slave/chip select line of the SPI device is still
 * active.
 * Multiple calls to spi_write_partial() can be done. To complete the
 * transfer to the SPI device, either execute spi_write() or spi_read()
 * at the end of the write sequence.
 *
 * If a concurrent transfer to the SPI device is already ongoing, this call
 * spins until the ongoing transfer is finished.
 *
 * @param spi_inst  SPI instance returned from spi_open()
 * @param buf       buffer that should be transfered
 * @param cnt       number of bytes that should be transfered
 *
 * @note    This is a synchronous function, i.e. function waits until command has completed.
 */
ssize_t spi_write_partial(void* spi_inst, const char* buf, size_t cnt,
        int ticks)
{
    DiagSPI* spi = (DiagSPI*) spi_inst;
    if (xSemaphoreTakeRecursive(spi->lock, ( portTickType ) ticks) != pdTRUE)
    {
        return -1;
    }
    spi->is_partial_transfer = true;
    ssize_t rv = spi_write_private(spi_inst, buf, cnt, ticks);
    xSemaphoreGiveRecursive(spi->lock);
    return rv;
}

/**
 * Write to SPI and receive data in the same cycle.
 *
 * This function is used for sending and receiving data from a
 * SPI device.
 *
 * If a concurrent transfer to the SPI device is already ongoing, this call
 * spins until the ongoing transfer is finished.
 *
 * @param spi_inst  SPI instance returned from spi_open()
 * @param tx_buf    buffer that should be transfered
 * @param tx_len    number of bytes that should be transfered
 * @param rx_buf    buffer that should be received
 * @param rx_len    number of bytes that should be received
 *
 * @param ticks     Timeout in system ticks
 *
 * @note    This is a synchronous function, i.e. function waits until command has completed.
 */
ssize_t spi_transmit(void* spi_inst, const char* tx_buf, size_t tx_len,
		char* rx_buf, size_t rx_len, int ticks)
{
	return max(rx_len,tx_len);
}


/**
 * Read from SPI.
 *
 * This call blocks until all requested bytes have been transfered to provided
 * buffer.
 *
 * @param spi_inst  SPI instance returned from spi_open()
 * @param buf       buffer that should be used for receiving bytes
 * @param cnt       number of bytes to receive. This value must not exceed
 *                  the max. size of buf
 * @param ticks     Timeout in system ticks
 *
 * @return          >0  number of bytes that have been received
 *                  -1  error, e.g. transfer timeout occured
 *
 * @note    This is a synchronous function, i.e. function waits until command has completed.
 */
ssize_t spi_read(void* spi_inst, char* buf, size_t cnt, int ticks)
{
    DiagSPI* spi = (DiagSPI*) spi_inst;
    if ((0 == cnt) || (NULL == spi))
        return 0;

    /* no concurrent access */
    if (xSemaphoreTakeRecursive(spi->lock, ( portTickType ) ticks) != pdTRUE)
    {
        return -1;
    }

    // reserve RX
    spi->rx_in_use = true;
    spi->tx_in_use = false;

    // reset partial transfer ongoing
    spi->is_partial_transfer = false;

    // we assume that cnt doesn't exceed max. buffer size of buf
    spi->rx_cnt = cnt;
    spi->tx_cnt = 0;
    spi->rx_pos = 0;
    spi->tx_pos = 0;
    spi->rx_buf = buf;

    if (spi->is_master)
    {
        spi_rx_fifo_clear(spi);
        spi_chip_select(spi->SPIx);
    }
    else
    {
        /* wait until a previous ongoing transfer is finished by master */
        while (spi_is_cs_active(spi))
        {
            ;
        }
    }

#ifndef SPI_USES_DMA
    spi_rx_isr_enable(spi->SPIx);
    if (spi->is_master)
    {
        spi_tx_isr_enable(spi->SPIx);
    }
#else
    spi_dma_rx_init(spi, buf, cnt);
#endif

    if (false == spi->is_master)
    {
        /* set Slave RDY signal */
        spi_host_com_set_imx_request_active();
    }

    ssize_t rv;
    // wait until ISR signals end of transfer
    if (xSemaphoreTake(spi->rx_sem, (portTickType) ticks) != pdTRUE)
    {
        /*disable isrs & dma */
    #ifdef SPI_USES_DMA
        spi_dma_rx_enable(spi, false);
    #else
        spi_rx_isr_disable(spi->SPIx);
        if (spi->is_master)
        {
            spi_tx_isr_disable(spi->SPIx);
        }
    #endif
        rv = -1;
    }
    else
    {
        rv = spi->rx_pos;
    }

    if ((spi->is_master) &&
        (false == spi->is_partial_transfer))
    {
        spi_chip_deselect(spi->SPIx);
    }
    else
    {
        spi_host_com_set_imx_request_passive();
    }

    spi->is_partial_transfer = false;
    spi->rx_in_use = false;

    /* reset transfer buffer: it has to be set every time before receiving */
    spi->transfer_buf_ptr = NULL;
    spi->transfer_buf_size = 0;
    xSemaphoreGiveRecursive(spi->lock);

    return rv;
}

/**
 * Sets the transfer buffer that is used for transmitting dummy bytes
 * via MOSI in spi master mode for the appropriate SPI interface.
 * This transfer buffer will be run in circular mode.
 *
 * @param spi_inst      SPI instance returned from spi_open()
 * @param buf           buffer that should be used to transfer dummy bytes
 * @param cnt
 *
 */
void spi_set_rx_transfer_buffer(void* spi_inst, char* buf, size_t cnt)
{
    DiagSPI* spi = (DiagSPI*) spi_inst;
    if ((0 == cnt) || (NULL == spi) || (NULL == buf))
        return;
    spi->transfer_buf_ptr = buf;
    spi->transfer_buf_size = cnt;
}


SPI_TypeDef* spi_get_spi_ptr(void* spi_inst)
{
    DiagSPI* spi = (DiagSPI*) spi_inst;
    return spi->SPIx;
}


bool spi_is_cs_active(void* spi_inst)
{
    DiagSPI* spi = (DiagSPI*) spi_inst;
    return (GPIO_ReadInputDataBit(spi->GPIOx_nss, spi->NSS) == Bit_RESET);
}
/* EOF */
